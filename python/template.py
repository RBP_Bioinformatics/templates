from optparse import OptionParser
from optparse import OptionGroup
import sys

usage = "usage: %prog [options] arg1 arg2"
opt = OptionParser(usage=usage)

# https://docs.python.org/3/library/optparse.html#generating-help

opt.add_option(
    "--ifile", 
    action="store", 
    help="Input table file, ommit for STDIN",
    default="-"
)
opt.add_option(
    "-v", "--verbose",
    action="store_true", 
    dest="verbose", 
    default=True,
    help="make lots of noise [default]"
)
opt.add_option(
    "-q", "--quiet",
    action="store_false", dest="verbose",
    help="be vewwy quiet (I'm hunting wabbits)"
)
opt.add_option(
    "-m", "--mode",
    default="intermediate",
    help="interaction mode: novice, intermediate, "
    "or expert [default: %default]"
)
group = OptionGroup(
    opt, "Group of Options",
        "These options are in their own group"
)
group.add_option("-g", action="store_true", help="Group option.")
group.add_option("-k", action="store_true", help="Group option 2.")
opt.add_option_group(group)

(options, args) = opt.parse_args()

def filehandle_for (filename): 
    if filename == "-":
        filehandle = sys.stdin
    else:
        filehandle = open(filename)
    return filehandle

#### checking for number of input args
#if len(args) != 1: 
    #parser.error("incorrect number of arguments")

#### verbose
#if options.verbose:
    #print "reading %s..." % options.filename

print options
print options.ifile

fh = filehandle_for(options.ifile)
for line in fh:
    print line




