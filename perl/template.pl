# POD documentation - main docs before the code

=head1 NAME

Name - Short description

=head1 SYNOPSIS

This script gives the xyz of the ... using zzz. It can be used as ... to do sxxs.

=head1 DESCRIPTION

Long description goes here.
Can be several lines long.
For examples of POD documentation see: https://perldoc.perl.org/perlpod.html

=cut

# Let the code begin...


#!/usr/bin/env perl

use Modern::Perl;
use autodie;
use Getopt::Long::Descriptive;

# Define and read command line options
my ($opt, $usage) = describe_options(
	"Usage: %c %o",
	["Name - Short description"],
	[],
	['ifile=s',
		'Input file name. Use - for STDIN',
		{required => 1}],
	['integerval=i',
		'Optional integer value',
	],
	['anotherint=i',
		'Integer value. Default=5',
		{default => 5}
	],
	['stringval=s',
		'simple string value',
	],
	['multival=s@',
		'this string value can be given multiple times. Any type specification ending in @ can be given multiple times'
	],
	['verbose|v', 'Print progress'],
	['help|h', 'Print usage and exit',
		{shortcircuit => 1}],
);
print($usage->text), exit if $opt->help;

my $foo_val = $opt->integerval; #getting input in variable
if (defined $opt->multival){ #checking if optional parameter exists
    my @foo_array = @{$opt->multival}; #getting multiple values into an array
}

my $IN = filehandle_for($opt->ifile);
while (my $line = <$IN>){
    chomp $line;
    say $line; #print with automatic newline
}
close $IN;

exit;

###subs

sub filehandle_for {
#   this opens a filehande by filename or opens STDIN if the filename is "-"
	my ($file) = @_;

	if ($file eq '-'){
		open(my $IN, "<-");
		return $IN;
	}
	else {
		open(my $IN, "<", $file);
		return $IN
	}
}
